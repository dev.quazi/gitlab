#!/usr/bin/python
#Automation of IAM software

#Introduction to program
print ("Welcome to AUTOIAM where we will completely configure your system to correlate your company's departments, employee logins, and security measures. Enter your company name.")
CORP_NAME = input()

#Begin collecting data; Departments
print ("Welcome "+CORP_NAME+", Begin with entering each department in your business.")
USER_INP = input()

#Funtion to automate list using loop 
LIST_NAME = list()
def LIST_ADD (USER_INP):
    LIST_NAME.clear
    while (USER_INP != "0"):       
        LIST_NAME.append(USER_INP)
        print("Enter Next Department or 0 to complete.")
        USER_INP = input()
        
LIST_ADD(USER_INP)
DEP_LIST = LIST_NAME

#Collecting data; Employees
print("Enter Employee")
USER_INP = input()

#Function to create list
EMP_DEP_LIST = list()
EMP_LIST = list()
def DATAB_LIST (USER_INP):   
    while (USER_INP != "0"):
        EMP_LIST.append (USER_INP)
        print("Which department is " + USER_INP + " in?")
        print(*DEP_LIST) 
        USER_INP = input()       
        EMP_DEP_LIST.append (USER_INP) 

        print("Enter Next Employee or 0 to complete.")
        USER_INP = input()

DATAB_LIST(USER_INP)
#Create Groups with list
import os 

def ADD_GROUPS_LINX (LISTN):
    for item in LISTN:
        os.system('groupadd ' + item)

ADD_GROUPS_LINX (DEP_LIST)

#Create Users
import random
import string

def RAND_PASS_GEN():
    CHARS = string.ascii_letters + string.punctuation
    return ''.join(random.choice(CHARS) for x in range (10))

def ADD_EMP_USR (EMPL, DEPL):
    x=0
    os.system('touch pass.txt')
    for item in EMPL:
        VAR_PASS = RAND_PASS_GEN()
        os.system ('useradd ' + item)
	os.system ('usermod -p ' + VAR_PASS + ' ' + item)
        os.system ('echo ' + item + ' ' + VAR_PASS + '  >> pass.txt')
        EMPDEP= DEPL[x]
        os.system ('usermod -g ' + EMPDEP + ' ' + item)
        x+=1
ADD_EMP_USR (EMP_LIST, EMP_DEP_LIST)
